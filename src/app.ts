import express, { Express } from "express";
import { Server } from "node:http";
import { UserController } from "./users/users.controller";
import { ExceptionFilter } from "./errors/exception.filter";
import { ILogger } from "./logger/logger.interface";
import { inject, injectable } from "inversify";
import { TYPES } from "./types";
import "reflect-metadata";
import { IConfigService } from "./config/config.service.interface";
import { IExceptionFilter } from "./errors/exception.filter.interface";
import { PrismaService } from "./db/prisma.service";
import { IUsersRepository } from "./users/users.repository.interface";
import {AuthMiddleware} from "./common/auth.middleware";

@injectable()
export class App {
	private readonly app: Express;
	private readonly port: number;
	private server: Server;

	constructor(
		@inject(TYPES.ILogger) private loggerService: ILogger,
		@inject(TYPES.IConfigService) private configService: IConfigService,
		@inject(TYPES.IUserController) private userController: UserController,
		@inject(TYPES.ExceptionFilter) private exceptionFilter: IExceptionFilter,
		@inject(TYPES.PrismaService) private prismaService: PrismaService,
		@inject(TYPES.IUsersRepository) private userService: IUsersRepository
	) {
		this.app = express();
		this.port = Number(this.configService.get("PORT"));
	}

	private useRoutes(): void {
		this.app.use("/users", this.userController.router);
	}

	private useMiddlewares(): void {
		const authMiddleware = new AuthMiddleware(this.configService.get("SECRET"));

		this.app.use(express.json());
		this.app.use(authMiddleware.execute);
	}

	private useExceptionFilter(): void {
		this.app.use(this.exceptionFilter.catch.bind(this.exceptionFilter));
	}

	public async init(): Promise<void> {
		this.useMiddlewares();
		this.useRoutes();
		this.useExceptionFilter();
		await this.prismaService.connect();
		this.server = this.app.listen(this.port);
		this.loggerService.log("Express HTTP server start listening...");
	}
}
