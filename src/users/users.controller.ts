import e, {NextFunction, Request, Response} from "express";
import {BaseController} from "../common/base.controller";
import {injectable, inject} from "inversify";
import {TYPES} from "../types";
import {ILogger} from "../logger/logger.interface";
import {IUserController} from "./users.controller.interface";
import {UserLoginDto} from "./dto/users-login.dto";
import {UserRegisterDto} from "./dto/user-register.dto";
import {IUserService} from "./users.service.interface";
import {IConfigService} from "../config/config.service.interface";
import {HTTPException} from "../errors/http.exception";
import {ValidateMiddleware} from "../common/validate.middleware";
import {sign} from "jsonwebtoken";
import "reflect-metadata";
import {AuthMiddleware} from "../common/auth.middleware";
import {AuthGuard} from "../common/auth.guard";

@injectable()
export class UserController extends BaseController implements IUserController {
    constructor(
        @inject(TYPES.ILogger) private loggerService: ILogger,
        @inject(TYPES.IUserService) private userService: IUserService,
        @inject(TYPES.IConfigService) private configService: IConfigService
    ) {
        super(loggerService);
        this.bindRouter([
            {
                path: "/login",
                method: "post",
                func: this.login,
                middlewares: [new ValidateMiddleware(UserLoginDto)]
            },
            {
                path: "/register",
                method: "post",
                func: this.register,
                middlewares: [new ValidateMiddleware(UserRegisterDto)]
            },
            {
                path: "/info",
                method: "get",
                func: this.info,
                middlewares: [new AuthGuard()]
            }
        ]);
    }

    public async login({body}: Request<{}, {}, UserLoginDto>, res: Response, next: NextFunction): Promise<void> {
        const result = await this.userService.validateUser(body);

        if (!result) {
            return next(new HTTPException(401, "Auth Exception", "login"));
        }

        const jwt = await this.signJWT(body.email, this.configService.get("SECRET"));

        this.ok(res, {
            success: result,
            jwt: jwt
        });
    }

    public async register({body}: Request<{}, {}, UserRegisterDto>, res: Response, next: NextFunction): Promise<void> {
        const result = await this.userService.createUser(body);

        if (!result) {
            return next(new HTTPException(422, "User already exists!", "/users/register"));
        }

        this.created(res, {email: result.email, id: result.id});
    }

    public async info({user}: Request, res: Response, next: NextFunction): Promise<void> {
        const userInfo = await this.userService.getUser(user);

        this.ok(res, {email: userInfo?.email, id: userInfo?.id});
    }

    private async signJWT(email: string, secret: string): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            sign({
                email,
                iat: Math.floor(Date.now() / 1000)
            }, secret, {
                algorithm: "HS256"
            }, (err, token) => {
                if (err) {
                    reject(err);
                }
                resolve(token as string);
            });
        });
    }
}
