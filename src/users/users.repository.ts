import { UserModel } from "@prisma/client";
import { User } from "./user.entity";
import { IUsersRepository } from "./users.repository.interface";
import { inject, injectable } from "inversify";
import "reflect-metadata";
import { TYPES } from "../types";
import { PrismaService } from "../db/prisma.service";

@injectable()
export class UsersRepository implements IUsersRepository {
    constructor(@inject(TYPES.PrismaService) private prismaService: PrismaService) {}
    
    public async create(user: User): Promise<UserModel> {
        return this.prismaService.client.userModel.create({
            data: {
                email: user.email,
                name: user.name,
                password: user.password,
            }
        });
    }

    public async find(email: string): Promise<UserModel | null> {
        return this.prismaService.client.userModel.findFirst({
            where: {
                email: email
            }
        });
    }

}