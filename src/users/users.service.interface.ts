import { UserModel } from "@prisma/client";
import { UserRegisterDto } from "./dto/user-register.dto";
import { UserLoginDto } from "./dto/users-login.dto";
import { User } from "./user.entity";

export interface IUserService {
    createUser(dto: UserRegisterDto):Promise<UserModel | null>;
    validateUser(dto: UserLoginDto): Promise<boolean>;
    getUser(email: string): Promise<UserModel | null>;
}