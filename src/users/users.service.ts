import { inject, injectable } from "inversify";
import { UserRegisterDto } from "./dto/user-register.dto";
import { UserLoginDto } from "./dto/users-login.dto";
import { User } from "./user.entity";
import { IUserService } from "./users.service.interface";
import "reflect-metadata";
import { TYPES } from "../types";
import { IConfigService } from "../config/config.service.interface";
import { IUsersRepository } from "./users.repository.interface";
import { UserModel } from "@prisma/client";

@injectable()
export class UserService implements IUserService {
    constructor(
        @inject(TYPES.IConfigService) private configService: IConfigService,
        @inject(TYPES.IUsersRepository) private usersRepository: IUsersRepository
    ) {}

    public async createUser({ email, name, password }: UserRegisterDto): Promise<UserModel | null> {
        const newUser = new User(email, name);
        const salt = Number(this.configService.get("SALT"));

        await newUser.setPassword(password, salt);

        const existedUser = await this.usersRepository.find(email);
        
        if (existedUser) {
            return null;
        }

        return this.usersRepository.create(newUser);
    }

    public async validateUser({ email, password }: UserLoginDto): Promise<boolean> {
        const existedUser = await this.usersRepository.find(email);

        if (!existedUser) {
            return false;
        }
        
        const user = new User(existedUser.email, existedUser.name, existedUser.password);
        
        return user.validatePassword(password);
    }

    getUser(email: string): Promise<UserModel | null> {
        return this.usersRepository.find(email);
    }

}