import { IsEmail, IsString, MinLength } from "class-validator";

export class UserLoginDto {
    @IsEmail({}, { message: "Email format is invalid" })
    public email: string;

    @MinLength(8, { message: "Password should have at least 8 characters"})
    @IsString({ message: "Password not provided" })
    public password: string;
}