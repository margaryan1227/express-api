import { IsEmail, IsString, MinLength } from "class-validator";

export class UserRegisterDto {
    @IsEmail({}, { message: "Email format is invalid" })
    public email: string;

    @MinLength(3, { message: "Name should have at least 3 letters"})
    @IsString({ message: "Name not provided" })
    public name: string;

    @MinLength(8, { message: "Password should have at least 8 characters"})
    @IsString({ message: "Password not provided" })
    public password: string;
}