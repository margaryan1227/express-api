import { NextFunction, Request, Response } from "express";
import { IExceptionFilter } from "./exception.filter.interface";
import { HTTPException } from "./http.exception";
import { inject, injectable } from "inversify";
import { ILogger } from "../logger/logger.interface";
import { TYPES } from "../types";
import "reflect-metadata";

@injectable()
export class ExceptionFilter implements IExceptionFilter {
	constructor(@inject(TYPES.ILogger) private logger: ILogger) {}

	public catch(err: Error | HTTPException, req: Request, res: Response, next: NextFunction): void {
		if (err instanceof HTTPException) {
			this.logger.error(`[${err.context}] Exception:${err.statusCode} ${err.message}`);
			res.status(err.statusCode).send({ err: err.message });
		} else {
			this.logger.error(`${err.message}`);
			res.status(500).send({ err: err.message });
		}
	}
}
