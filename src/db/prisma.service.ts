import { PrismaClient } from "@prisma/client";
import { inject, injectable } from "inversify";
import { ILogger } from "../logger/logger.interface";
import { TYPES } from "../types";
import "reflect-metadata";

@injectable()
export class PrismaService {
    public client: PrismaClient;

    constructor(@inject(TYPES.ILogger) private loggerService: ILogger) {
        this.client = new PrismaClient();
    }

    public async connect(): Promise<void> {
        try {
            await this.client.$connect();
            this.loggerService.log("[PrismaService] successfully connected to DB!")
        } catch(err) {
            if (err instanceof Error) {
                this.loggerService.error("[PrismaService] DB connection error!", err.message);
            }
        }
    }
    
    public async disconnect(): Promise<void> {
        await this.client.$disconnect();
        this.loggerService.log("[PrismaService] successfully disconnected to DB!")
    }
}