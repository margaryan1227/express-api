import {Request, Response, NextFunction} from "express";
import {IMiddleware} from "./middleware.interface";
import {JwtPayload, verify} from "jsonwebtoken";

export class AuthMiddleware implements IMiddleware {
    constructor(private readonly _secret: string) {
    }

    execute(req: Request, res: Response, next: NextFunction): void {
        if (req.headers.authorization) {
            const token = req.headers.authorization.split(" ")[1];
            verify(token, this._secret, (err, payload) => {
                if (err) {
                    return next();
                } else if (payload) {
                    req.user = (payload as JwtPayload).email;
                    return next();
                }
            });
        }
        return next();
    }

}