# Express-API
Example of REST API in Express and Typescript which is easy to scale and develop because of Layered architecture and DI.


## Getting started

* npm install

# Generate DB Models
npm run generate

# For development 
* npm run dev 

# For production
* npm run build
* npm run start

# ESLint
* npm run lint
* npm run lint:fix

# Diagnose memory and CPU usage
* npm run dev:inspect
